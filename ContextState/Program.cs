﻿using System;

namespace ContextState
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new AppDbContext();

            context.Database.EnsureCreated();

            Insert();

            Update();

            Delete();
        }

        private static void Delete()
        {
            using (var context = new AppDbContext())
            {
                var teacher = new Teacher
                {
                    Id = 3
                };

                Console.WriteLine(context.Entry(teacher).State);

                context.Teachers.Remove(teacher);

                Console.WriteLine(context.Entry(teacher).State);

                context.SaveChanges();

                Console.WriteLine(context.Entry(teacher).State);
            }
        }

        private static void Update()
        {
            using (var context = new AppDbContext())
            {
                var teacher = context.Teachers.Find(2);

                Console.WriteLine(context.Entry(teacher).State);

                teacher.DegreeType = DegreeType.Senior;

                Console.WriteLine(context.Entry(teacher).State);

                context.SaveChanges();

                Console.WriteLine(context.Entry(teacher).State);
            }
        }

        private static void Insert()
        {
            using (var context = new AppDbContext())
            {
                var teacher = new Teacher
                {
                    Name = "Amin",
                    DegreeType = DegreeType.Jonior,
                    Age = 35
                };

                Console.WriteLine(context.Entry(teacher).State);

                context.Teachers.Add(teacher);

                Console.WriteLine(context.Entry(teacher).State);

                context.SaveChanges();

                Console.WriteLine(context.Entry(teacher).State);
            }
        }
    }
}
