﻿namespace ContextState
{
    public class Teacher
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DegreeType DegreeType { get; set; }

        public int Age { get; set; }
    }
}
