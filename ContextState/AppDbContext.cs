﻿using Microsoft.EntityFrameworkCore;

namespace ContextState
{
    public class AppDbContext : DbContext
    {
        public DbSet<Teacher> Teachers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=.;Database=StateDb;Trusted_Connection=true;");
            
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Teacher>().ToTable("Teacher");

            modelBuilder.Entity<Teacher>().Property(p => p.Name).HasMaxLength(20);
            
            base.OnModelCreating(modelBuilder);
        }
    }
}
